package BasicHTTP;

import java.io.*;
import java.net.*;
import java.util.*;

public class BasicHTTP {

    public static String getHttpContent(String string){
        String content = "";

        try{
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();
        }
        catch(Exception e){
            System.err.println(e.toString());
        }
        return content;
    }

    public static Map getHttpHeaders(String string){
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();
        }
        catch (Exception e){
            System.err.println(e.toString());
        }
        return hashmap;
    }

    public static void main(String[] args) {
        System.out.println("Enter a website (eg. google.com):");
        Scanner input = new Scanner(System.in);
        String site = input.nextLine();
        System.out.println("\n-----------------HTTP-----------------");
        System.out.println(BasicHTTP.getHttpContent("http://" + site));
        System.out.println("\n-----------------MAP-----------------");
        Map<Integer, String> m = BasicHTTP.getHttpHeaders("http://" + site);

        for (Map.Entry<Integer, String> entry : m.entrySet()){
            System.out.println("Key = " + entry.getKey());
        }
    }
}
