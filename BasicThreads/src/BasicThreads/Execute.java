package BasicThreads;

import java.util.concurrent.*;

public class Execute {

    public static void main(String[] args){

        ExecutorService myService = Executors.newFixedThreadPool(3);

        Main m1 = new Main("Sleepy Virus");
        Main m2 = new Main("Tired Virus");
        Main m3 = new Main("Grumpy Virus");
        Main m4 = new Main("Sad Virus");
        Main m5 = new Main("Clever Virus");

        myService.execute(m1);
        myService.execute(m2);
        myService.execute(m3);
        myService.execute(m4);
        myService.execute(m5);

        myService.shutdown();
    }
}
