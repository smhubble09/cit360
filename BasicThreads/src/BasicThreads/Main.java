package BasicThreads;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Main implements Runnable {

    private String name;
    private int age;
    private int sleep;
    private int rand;
    AtomicInteger count;

    public Main(String name){
        this.name = name;

        Random random = new Random();
        this.rand = random.nextInt(1000);
        this.age = random.nextInt(100);
        this.sleep = random.nextInt(1000);
    }

    public void run(){
        System.out.println("\n\nInstalling " + name + ". Age = "
        + age + ".\nThe virus is tired, so it's going to sleep for " + sleep + " milliseconds. Random number: " + rand + "\n\n");
        for (count = new AtomicInteger(1); count.intValue() < rand; count.incrementAndGet()){
            if (count.intValue() % age == 0) {
                System.out.println(name + " is sleeping.");
                try {
                    Thread.sleep(sleep);
                }
                catch (InterruptedException e){
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + name + " has finished installing.\n\n");
    }
}
