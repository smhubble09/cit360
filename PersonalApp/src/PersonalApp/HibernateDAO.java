package PersonalApp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.lang.module.Configuration;
import java.time.LocalDate;
import java.util.*;

/** HibernateDAO implemented using a singleton pattern
 *  Used to get customer data from my MYSQL database*/
public class HibernateDAO {

    SessionFactory factory = null;
    Session session = null;

    private static HibernateDAO single_instance = null;

    private HibernateDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static HibernateDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new HibernateDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Movies> getMovies() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from PersonalApp.Movies";
            List<Movies> cs = (List<Movies>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to update Availability of movie */
    public void notAvailable(int movie, String cust, int day){
        try {
            LocalDate localDate = LocalDate.now();
            session = factory.openSession();
            Transaction tx = session.beginTransaction();
            Movies movies = session.load(Movies.class, movie);
            movies.setAvailable(false);
            movies.setCustomer(cust);
            movies.setDate(localDate);
            movies.setReturndate(localDate.plusDays(day));
            session.update(movies);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /** Used to update Availability of movie */
    public void isAvailable(int movie){
        try {
            session = factory.openSession();
            Transaction tx = session.beginTransaction();
            Movies movies = session.load(Movies.class, movie);
            movies.setAvailable(true);
            movies.setCustomer(null);
            movies.setDate(null);
            movies.setReturndate(null);
            session.update(movies);
            tx.commit();

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /** Used to get a single customer from database */
    public Movies getMovie(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from PersonalApp.Movies where id=" + id;
            Movies m = (Movies)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return m;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}