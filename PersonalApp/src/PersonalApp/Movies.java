package PersonalApp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "movies")
public class Movies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id){
        this.id = id;
    }

    @Column(name = "title")
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }

    @Column(name = "year")
    private int year;
    public int getYear() {
        return year;
    }
    public void setYear(int year){
        this.year = year;
    }

    @Column(name = "rating")
    private String rating;
    public String getRating() {
        return rating;
    }
    public void setRating(String rating){
        this.rating = rating;
    }

    @Column(name = "price")
    private double price;
    public double getPrice() {
        return price;
    }
    public void setPrice(double price){
        this.price = price;
    }

    @Column(name = "customer")
    private String customer;
    public String getCustomer() {
        return customer;
    }
    public void setCustomer(String customer){
        this.customer = customer;
    }

    @Column(name = "date")
    private LocalDate date;
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date){
        this.date = date;
    }

    @Column(name = "returndate")
    private LocalDate returndate;
    public LocalDate getReturndate() {
        return returndate;
    }
    public void setReturndate(LocalDate returndate){
        this.returndate = returndate;
    }

    @Column(name = "available")
    private boolean available;
    public boolean getAvailable() {
        return available;
    }
    public void setAvailable(boolean available){
        this.available = available;
    }

    public String toString(){
        return String.valueOf(System.out.format("%3d%60s%15d%8s%2s%3.2f%2s",id,title,year,rating,"",price,""));
        /*return id + " - " + title + " - " + year + " - " + rating + " - $" + price + " - " +
                customer + " - " + date + " - " + returndate + " - " + available;*/
    }
}
