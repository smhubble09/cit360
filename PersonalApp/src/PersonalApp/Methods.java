package PersonalApp;

import java.lang.module.Configuration;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Methods {

    public void viewAvailableMovies(){
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movies> m = h.getMovies();
        System.out.println("\n\nAvailable movies:");
        System.out.format("%3s%40s%35s%8s%2s%5s%2s", "ID","Title", "Release Date", "Rating", "", "Price", "\n");
        for (Movies i : m) {
            if (i.getAvailable())
                System.out.println(i);
        }
    }

    public void viewUnAvailableMovies(){
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movies> m = h.getMovies();
        System.out.println("\n\nAvailable movies:");
        System.out.format("%3s%40s%35s%8s%2s%5s%2s", "ID","Title", "Release Date", "Rating", "", "Price", "\n");
        for (Movies i : m) {
            if (!i.getAvailable())
                System.out.println(i);
        }
        System.out.println("0 - Exit");
    }

    public void rentMovie(int choice){
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movies> m = h.getMovies();
        for (Movies i : m) {
            if (i.getId() == choice){
                System.out.println("\nEnter Customer Name:");
                String customer = userInput();
                System.out.println("\nEnter Number of Days Renting:");
                int day = userInput2();
                h.notAvailable(choice, customer, day);
                System.out.println("\nYou have rented " + i.getTitle());
                System.out.println("\nYour total is $" + (day*totalPrice(choice)));
            }
            else if (choice == 0) {
                System.out.println("\n\nGoodbye");
                System.exit(0);
            }
        }
    }

    public void returnMovie(int choice){
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movies> m = h.getMovies();
        for (Movies i : m) {
            if (i.getId() == choice){
                h.isAvailable(choice);
                System.out.println("\nYou have returned " + i.getTitle());
            }
            else if (choice == 0) {
                System.out.println("\n\nGoodbye");
                System.exit(0);
            }
        }
    }

    public String userInput(){
        String customer = "";
        boolean correct = false;
        Scanner input = new Scanner(System.in);
        while (!correct){
            customer = input.nextLine();
            int l=0; // counter for number of letters
            int sp=0; // counter for number of spaces
            if (customer == null) {// checks if the String is null
                correct = false;
                System.out.println("Please enter a name");
            }
            for (int i = 0; i < customer.length(); i++) {
                if ((Character.isLetter(customer.charAt(i)))) {
                    l++;
                }
                if(customer.charAt(i) == ' ') {
                    sp++;
                }
            }
            if(sp==0 || l==0 ) { // even if one of them is zero then returns false
                correct = false;
                System.out.println("Enter only letters.");
            }
            else
                correct = true;
        }

        return customer;
    }

    public int userInput2(){
        int day = 0;
        boolean correct = false;
        Scanner input = new Scanner(System.in);
        while (!correct) {
            try {
                day = input.nextInt();
                correct = true;
            } catch (InputMismatchException e) {
                System.out.println("Incorrect input. ONLY ENTER NUMBERS.");
                input.next();
            }
        }

        return day;
    }

    private double totalPrice(int days){
        double price = 0;
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movies> m = h.getMovies();
        for (Movies i : m) {
            if (i.getId() == days)
                price = h.getMovie(days).getPrice();
            else
                price = 0;
        }
        return price;
    }
}
