package PersonalApp;

import com.mysql.cj.x.protobuf.MysqlxPrepare;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {

    public static void main(String[] args) {

        boolean program = false;
        while (!program) {

            //User Input
            Scanner input = new Scanner(System.in);
            int use = 0;
            System.out.println("\nWould you like to:");
            System.out.println("1 - Use program");
            System.out.println("2 - Exit");
            try {
                use = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Incorrect input. ONLY ENTER NUMBERS.");
                input.next();
            }
            if (use == 1) {

                System.out.println("\nEnter the number of what you would like to do:");
                System.out.println("0 - Exit");
                System.out.println("1 - View available movies");
                System.out.println("2 - Rent movie");
                System.out.println("3 - Return movie");

                boolean correct = false;
                boolean correct2 = false;
                boolean correct3 = false;
                int choice = 0;
                int moviechoice = 0;
                int returnmovie = 0;

                // Enter Choice
                while (!correct) {
                    Methods method = new Methods();
                    try {
                        choice = input.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.println("Incorrect input. ONLY ENTER NUMBERS.");
                        input.next();
                    }
                    int id = 0;
                    //View Available Movies
                    if (choice == 1) {
                        method.viewAvailableMovies();
                        correct = true;

                        //Rent Movie
                    } else if (choice == 2) {
                        method.viewAvailableMovies();
                        while (!correct2) {
                            try {
                                System.out.println("0 - Exit");
                                System.out.println("\nWhat movie would you like to rent? (Enter Movie ID)");
                                moviechoice = input.nextInt();
                            } catch (InputMismatchException e) {
                                System.out.println("Incorrect input. ONLY ENTER NUMBERS.");
                                input.next();
                            }
                            method.rentMovie(moviechoice);
                            correct2 = true;
                        }
                        correct = true;

                        //Return Movie
                    } else if (choice == 3) {
                        method.viewUnAvailableMovies();
                        while (!correct3) {
                            try {
                                System.out.println("\nWhat movie would you like to return? (Enter Movie ID)");
                                returnmovie = input.nextInt();
                            } catch (InputMismatchException e) {
                                System.out.println("Incorrect input. ONLY ENTER NUMBERS.");
                                input.next();
                            }
                            method.returnMovie(returnmovie);
                            correct3 = true;
                        }
                        correct = true;

                        //Invalid input
                    }
                    else if (choice == 0) {
                        System.out.println("\n\nGoodbye");
                        System.exit(0);
                    }
                    else {
                        System.out.println("Incorrect input. Try again.");
                        correct = false;
                    }
                }
            }
            else if (use == 2){
                System.out.println("\n\nGoodbye");
                program = true;
            }
            else
                System.out.println("\nIncorrect input. Try again.");
        }
        System.exit(0);
    }
}

