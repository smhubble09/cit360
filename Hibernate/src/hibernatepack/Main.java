package hibernatepack;

import java.util.*;
import java.util.List;

public class Main {

    public static void main(final String[] args){
        HibernateDAO h = HibernateDAO.getInstance();

        List<Movie> m = h.getMovies();
        for (Movie i : m)
            System.out.println(i);

        System.out.println("\nEnter an id: (1, 2 or 3)");
        Scanner input = new Scanner(System.in);
        int id = 0;
        int x = 0;

        while (x == 0) {
            id = input.nextInt();
            if (id == 1 || id == 2 || id == 3) {
                x = 1;
            }
            else
                System.out.println("Invalid id. Please try again.");
        }
        System.out.println(h.getMovie(id));
    }
}