package hibernatepack;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

/** HibernateDAO implemented using a singleton pattern
 *  Used to get customer data from my MYSQL database*/
public class HibernateDAO {

    SessionFactory factory = null;
    Session session = null;

    private static HibernateDAO single_instance = null;

    private HibernateDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static HibernateDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new HibernateDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Movie> getMovies() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernatepack.Movie";
            List<Movie> cs = (List<Movie>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database */
    public Movie getMovie(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernatepack.Movie where id=" + id;
            Movie m = (Movie)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return m;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
