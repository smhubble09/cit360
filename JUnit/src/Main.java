public class Main {
    private String name;
    private boolean happy = false;
    private String greeting;
    private String yourGreeting;
    private String dogGreeting;

    public Main(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void playGame() {
        happy = true;
    }

    public void setGreet(String greeting) {
        this.greeting = greeting;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setYourGreet(String yourGreeting) {
        this.yourGreeting = yourGreeting;
    }

    public String getYourGreeting() {
        return yourGreeting;
    }

    public void setDogGreet(String dogGreeting) {
        this.dogGreeting = dogGreeting;
    }

    public String getDogGreet() {
        return dogGreeting;
    }

}
