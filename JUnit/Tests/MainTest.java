import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private Main myName = new Main("Shane");
    private double[] array1 = {2.2,8.3,6.4,7.2};
    private double[] array2 = array1;
    public static void assertThat(int actual, int matcher){
    }

    @Test
    void nameEquals(){
        assertEquals("Shane", myName.getName());
    }

    @Test
    void happyFalse() {
        assertFalse(myName.isHappy());
    }

    @Test
    void happyTrue() {
        myName.playGame();
        assertTrue(myName.isHappy());
    }

    @Test
    void testArrayEqual() {
        assertArrayEquals(array1, array2);
    }

    @Test
    void greetingNotNull() {
        myName.setGreet("Hello");
        assertNotNull("Hello", myName.getGreeting());
    }

    @Test
    void greetingNotSame() {
        myName.setYourGreet("Hi");
        assertNotSame("Hello", myName.getYourGreeting());
    }

    @Test
    void dogGreetingNull() {
        myName.setDogGreet("");
        assertNull(null, myName.getDogGreet());
    }

    @Test
    void greetingSame() {
        myName.setYourGreet("Hello");
        assertSame("Hello", myName.getYourGreeting());
    }

    @Test
    void testThat() {
        assertThat(0,1);
    }

}