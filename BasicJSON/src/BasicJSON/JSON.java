package BasicJSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class JSON {

    public static String customerToJSON(Customer customer){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        }
        catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return s;
    }

    public static Customer JSONtoCustomer (String s){
        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        }
        catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return customer;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Customer cust = new Customer();

        //Enter customer name
        System.out.println("Customer name:");
        String name = input.nextLine();
        cust.setName(name);

        //Enter address
        System.out.println("Customer address:");
        String address = input.nextLine();
        cust.setAddress(address);

        //Enter phone number
        boolean correct = false;
        long phone = 0;
        while (!correct){ //Verify input is correct
            try{
                System.out.println("Customer phone number:");
                phone = input.nextLong();
                correct = true;
            }
            catch (InputMismatchException e){
                System.out.println("Invalid input. Only input numbers.");
                input.next();
            }
        }
        cust.setPhone(phone);

        //Enter age
        correct = false;
        int age = 0;
        while (!correct){ //Verify input is correct
            try{
                System.out.println("Customer age:");
                age = input.nextInt();
                correct = true;
            }
            catch (InputMismatchException e){
                System.out.println("Invalid input. Only input numbers.");
                input.next();
            }
        }
        cust.setAge(age);

        String json = JSON.customerToJSON(cust);
        System.out.println(json);

        Customer cust2 = JSON.JSONtoCustomer(json);
        System.out.println(cust2);
    }
}
