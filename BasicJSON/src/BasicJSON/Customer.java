package BasicJSON;

public class Customer {

    private String name;
    private String address;
    private  long phone;
    private int age;

    //Get and set name
    public  String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    //Get and set phone
    public long getPhone(){
        return phone;
    }
    public void setPhone(long phone){
        this.phone = phone;
    }

    //Get and set address
    public String getAddress(){
        return address;
    }
    public void setAddress(String address){
        this.address = address;
    }

    //Get and set age
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }

    public String toString(){
        return "Name: " + name + "\nAddress: " + address + "\nPhone: " + phone + "\nAge: " + age;
    }
}
