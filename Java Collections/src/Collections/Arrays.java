package Collections;

import java.util.*;

public class Arrays {

    public static void main() {

        List<String> movies = new ArrayList<>();
        movies.add("Star Wars: Episode IV - A New Hope");
        movies.add("Star Wars: Episode V - The Empire Strikes Back");
        movies.add("Star Wars: Episode VI - Return of the Jedi");
        movies.add("Batman Begins");
        movies.add("The Dark Knight");
        movies.add("The Dark Knight Rises");

        System.out.println("--List--");
        for (Object str : movies){
            System.out.println(str);
        }
        Scanner input = new Scanner(System.in);

        int x = 0;
        while (x == 0) {
            System.out.println("Would you like to add or delete a movie?");
            String choice = input.nextLine().toLowerCase();
            switch (choice){
                case "add":
                    x = 1;
                    System.out.println("Name of the new movie:");
                    String newMovie = input.nextLine();
                    movies.add(newMovie);
                    break;
                case "delete":
                    x = 1;
                    System.out.println("What movie would you like to delete? (Use a number)");
                    System.out.println("1 - Star Wars: Episode IV");
                    System.out.println("2 - Star Wars: Episode V");
                    System.out.println("3 - Star Wars: Episode VI");
                    System.out.println("4 - Batman Begins");
                    System.out.println("5 - The Dark Knight");
                    System.out.println("6 - The Dark Knight Rises");
                    Integer number = input.nextInt();
                    switch (number){
                        case 1:
                            movies.remove("Star Wars: Episode IV - A New Hope");
                            break;
                        case 2:
                            movies.remove("Star Wars: Episode V - The Empire Strikes Back");
                            break;
                        case 3:
                            movies.remove("Star Wars: Episode VI - Return of the Jedi");
                            break;
                        case 4:
                            movies.remove("Batman Begins");
                            break;
                        case 5:
                            movies.remove("The Dark Knight");
                            break;
                        case 6:
                            movies.remove("The Dark Knight Rises");
                            break;
                        default:
                            System.out.println("Incorrect input. System closing");
                            System.exit(0);
                    }
                    break;
                default:
                    System.out.println("Incorrect input.\n");
            }
        }
        System.out.println("--New List--");
        for (Object str : movies){
            System.out.println(str);
        }
    }
}
