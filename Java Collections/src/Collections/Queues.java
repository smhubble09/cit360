package Collections;

import java.util.*;

public class Queues {

    public static void main() {

        Queue<String> character = new PriorityQueue<>();
        character.add("Luke Skywalker");
        character.add("Han Solo");
        character.add("Princess Leia");
        character.add("Batman");
        character.add("Alfred");
        character.add("Jim Gordon");

        System.out.println("\n--Queue--");
        System.out.println(character);
        Scanner input = new Scanner(System.in);

        int x = 0;
        while (x == 0) {
            System.out.println("Would you like to add or delete a character?");
            String choice = input.nextLine().toLowerCase();
            switch (choice){
                case "add":
                    x = 1;
                    System.out.println("Name of the new character:");
                    String newCharacter = input.nextLine();
                    character.add(newCharacter);
                    break;
                case "delete":
                    x = 1;
                    character.remove();
                    break;
                default:
                    System.out.println("Incorrect input.\n");
            }
        }
        System.out.println("--New Queue--");
        System.out.println(character);
    }
}
