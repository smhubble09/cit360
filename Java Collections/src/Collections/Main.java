package Collections;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int x = 0;
        while (x == 0) {
            System.out.println("What would you like? (Use number)");
            System.out.println("1 - Arrays\n2 - Sets\n3 - Queues\n4 -Maps");
            Integer choice = input.nextInt();
            switch (choice) {
                case 1:
                    x = 1;
                    Arrays.main();
                    break;
                case 2:
                    x = 1;
                    Sets.main();
                    break;
                case 3:
                    x = 1;
                    Queues.main();
                    break;
                case 4:
                    x = 1;
                    Maps.main();
                    break;
                default:
                    System.out.println("You didn't enter a correct option.\n");
            }
        }
    }
}