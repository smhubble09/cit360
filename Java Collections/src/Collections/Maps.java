package Collections;

import java.util.*;

public class Maps {

    public static void main() {

        Map<Integer, String> vil = new HashMap<>();
        vil.put(1, "Darth Vader");
        vil.put(2, "Governor Tarkin");
        vil.put(3, "Emperor Palpatine");
        vil.put(4, "Ra's Al Ghul");
        vil.put(5, "The Joker");
        vil.put(6, "Bane");

        System.out.println("\n--Map--");
        for (int i = 1; i < 7; i++) {
            String result = vil.get(i);
            System.out.println(result);
        }
        Scanner input = new Scanner(System.in);

        int x = 0;
        while (x == 0) {
            System.out.println("Would you like to add or delete an villian?");
            String choice = input.nextLine().toLowerCase();
            switch (choice){
                case "add":
                    x = 1;
                    System.out.println("Name of the new villian:");
                    String newVil = input.nextLine();
                    vil.put(7, newVil);
                    break;
                case "delete":
                    x = 1;
                    System.out.println("What villian would you like to delete? (Use a number)");
                    System.out.println("1 - Darth Vader");
                    System.out.println("2 - Governor Tarkin");
                    System.out.println("3 - Emperor Palpatine");
                    System.out.println("4 - Ra's Al Ghul");
                    System.out.println("5 - The Joker");
                    System.out.println("6 - Bane");
                    Integer number = input.nextInt();
                    switch (number){
                        case 1:
                            vil.remove(1);
                            break;
                        case 2:
                            vil.remove(2);
                            break;
                        case 3:
                            vil.remove(3);
                            break;
                        case 4:
                            vil.remove(4);
                            break;
                        case 5:
                            vil.remove(5);
                            break;
                        case 6:
                            vil.remove(6);
                            break;
                        default:
                            System.out.println("Incorrect input. System closing");
                            System.exit(0);
                    }
                    break;
                default:
                    System.out.println("Incorrect input.\n");
            }
        }
        System.out.println("--New Set--");
        System.out.println(vil);
    }
}
