package Collections;

import java.util.*;

public class Sets {

    public static void main() {

        Set<String> act = new TreeSet<>();
        act.add("Mark Hamill");
        act.add("Harrison Ford");
        act.add("Carrie Fisher");
        act.add("Christian Bale");
        act.add("Michael Caine");
        act.add("Gary Oldman");

        System.out.println("\n--Set--");
        for (Object str : act){
            System.out.println(str);
        }
        Scanner input = new Scanner(System.in);

        int x = 0;
        while (x == 0) {
            System.out.println("Would you like to add or delete an actor?");
            String choice = input.nextLine().toLowerCase();
            switch (choice){
                case "add":
                    x = 1;
                    System.out.println("Name of the new actor:");
                    String newActor = input.nextLine();
                    act.add(newActor);
                    break;
                case "delete":
                    x = 1;
                    System.out.println("What actor would you like to delete? (Use a number)");
                    System.out.println("1 - Mark Hamill");
                    System.out.println("2 - Harrison Ford");
                    System.out.println("3 - Carrie Fisher");
                    System.out.println("4 - Christian Bale");
                    System.out.println("5 - Michael Caine");
                    System.out.println("6 - Gary Oldman");
                    Integer number = input.nextInt();
                    switch (number){
                        case 1:
                            act.remove("Mark Hamill");
                            break;
                        case 2:
                            act.remove("Harrison Ford");
                            break;
                        case 3:
                            act.remove("Carrie Fisher");
                            break;
                        case 4:
                            act.remove("Christian Bale");
                            break;
                        case 5:
                            act.remove("Michael Caine");
                            break;
                        case 6:
                            act.remove("Gary Oldman");
                            break;
                        default:
                            System.out.println("Incorrect input. System closing");
                            System.exit(0);
                    }
                    break;
                default:
                    System.out.println("Incorrect input.\n");
            }
        }
        System.out.println("--New Set--");
        for (Object str : act){
            System.out.println(str);
        }
    }
}
